package model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Reservation {
	
	private int idreservation;
	private int idcustomer;
	private String idroom;
	private String roomtyp;
	private String bedtyp;
	private String date;
	private String checkIn;
	private Date checkInDate;
	private String checkOut;
	private Date checkOutDate;
	private String countPerson;
	private String service;
	private String price;
	private double totalPrice;
	
	// Dieser Konsturkt erzeugt ein Reservierungsobjekt
	
	public Reservation (int idcustomer,String idroom, String service, String countPerson, java.util.Date checkIn, java.util.Date checkOut){
		this.idcustomer = idcustomer;
		this.idroom = idroom;
		this.service = service;
		this.countPerson = countPerson;
		this.checkInDate = new java.sql.Date(checkIn.getTime());	//Typkonventierung
		this.checkOutDate = new java.sql.Date(checkOut.getTime());
		System.out.print(checkOut);
		this.totalPrice();
		
		
	}

	private void totalPrice() {
		
		
	}
		
		
		
	
	

	
	// GETTER 
	
	public int getIdreservation() {
		return idreservation;
	}

	public String getIdroom() {
		return idroom;
	}

	public String getRoomtyp() {
		return roomtyp;
	}

	public String getBedtyp() {
		return bedtyp;
	}

	public String getCheckIn() {
		return checkIn;
	}

	public Date getCheckInDate() {
		return checkInDate;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public Date getCheckOutDate() {
		return checkOutDate;
	}

	public String getCountPerson() {
		return countPerson;
	}

	public String getService() {
		return service;
	}

	public String getPrice() {
		return price;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public int rowsInResultSet(ResultSet r) throws SQLException {
		r.last();
		int rows = r.getRow();
		r.beforeFirst();
		return rows;
	}
	
	
	
	
	
	
	
	
	
	
}
