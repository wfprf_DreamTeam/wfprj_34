package model;

import java.util.ArrayList;

public class Customer {
	private int idcustomer;
	private String firstName;
	private String lastName;
	private String street;
	private String dateOfBirth;
	private String gender;
	private String customerPhone;
	private String nationality;
	private String customerEmai;
	private String customerIdentityCard;
	private int adressid;


public Customer (String firstName,String lastName,String customerPhone, String customerEmai,
		String street,  String gender, String nationality){
	this.firstName = firstName;
	this.lastName = lastName;
	this.customerPhone = customerPhone;
	this.customerEmai = customerEmai;
	this.street = street;
	this.gender = gender;
	this.nationality = nationality;

	
}

public Customer(){
	
}

public int getIdcustomer() {
	return idcustomer;
}

public void setIdcustomer(int idcustomer) {
	this.idcustomer = idcustomer;
}

public String getFirstName() {
	return firstName;
}

public void setFirstName(String firstName) {
	this.firstName = firstName;
}

public String getLastName() {
	return lastName;
}

public void setLastName(String lastName) {
	this.lastName = lastName;
}

public String getStreet() {
	return street;
}

public void setStreet(String street) {
	this.street = street;
}

public String getDateOfBirth() {
	return dateOfBirth;
}

public void setDateOfBirth(String dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}

public String getGender() {
	return gender;
}

public void setGender(String gender) {
	this.gender = gender;
}

public String getCustomerPhone() {
	return customerPhone;
}

public void setCustomerPhone(String customerPhone) {
	this.customerPhone = customerPhone;
}

public String getNationality() {
	return nationality;
}

public void setNationality(String nationality) {
	this.nationality = nationality;
}

public String getCustomerEmai() {
	return customerEmai;
}

public void setCustomerEmai(String customerEmai) {
	this.customerEmai = customerEmai;
}

public String getCustomerIdentityCard() {
	return customerIdentityCard;
}

public void setCustomerIdentityCard(String customerIdentityCard) {
	this.customerIdentityCard = customerIdentityCard;
}

public int getAdressid() {
	return adressid;
}

public void setAdressid(int adressid) {
	this.adressid = adressid;
}




}
