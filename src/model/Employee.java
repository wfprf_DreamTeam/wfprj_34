package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/** 
 * Diese Klasse erstellt einen Mitarbeiter.
 */
public class Employee {

	private int idemployee = -1;
	private String firstName = null;
	private String lastName = null;
	private String street = null;
	private String tel = null;
	private String mobil = null;
	private String email = null;
	private String dayOfBirth = null;
	private String sex = null;
	private String nationality = null;
	
	private int zip = 0;
	private String city = null;
	private String country = null;
	
	private String depositor = null;
	private String credit_Institution = null;
	private int accountNr;
	private int bin;
	
	
	/**
	 * Konstuktor f�r MitarbeiterEditieren (mit ID)
	 * @param ID des Mitarbeiters (int)
	 */
	public Employee(int id){
		//Daten einlesen aus der DABA und die angelegten Klassen
		super();
		//DABA Connection
		Connection cone = ConnectionFactory.createConnection();
		try{
			Statement sta = cone.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet res = sta.executeQuery(
		"SELECT  employee.firstName, employee.lastName, employee.street, employee.tel, employee.mobil," +
				"employee.email, employee.dayOfBirth, employee.sex, employee.nationality," + 
				"emAddress.zip, emAddress.city, emAddress.country," + 
				"emBankaccount.depositor, emBankaccount.credit_Institution, emBankaccount.accountNr, emBankaccount.bin" + 
		"FROM employee Join emaddress on (employee.emAddress_idemAddress = emAddress.idemAddress)" +
			  "Join emBankaccount on (employee. emBankaccount_idemBankaccount = emBankaccount.idemBankaccount)" +
	    "WHERE employee.idemployee = employee.idemployee group by employee.idemployee" + Integer.toString(id) + ";");
			int nbOfRows = rowsInResultSet(res);
			res.first();
			if (nbOfRows != 0)	{
				this.idemployee = id;
				this.firstName = res.getString("employee.firstName");
				this.lastName = res.getString("employee.lastName");
				this.street = res.getString("employee.street");
				this.tel = res.getString("employee.tel");
				this.mobil = res.getString("employee.mobil");
				this.email = res.getString("employee.email");
				this.dayOfBirth = res.getString("employee.dayOfBirth");
				this.sex = res.getString("employee.sex");
				this.nationality = res.getString("employee.nationality");
				this.zip = res.getInt("emAddress.zip");
				this.city = res.getString("emAddress.city");
				this.country = res.getString("emAddress.country");
				this.depositor = res.getString("emBankaccount.depositor");
				this.credit_Institution = res.getString("emBankaccount.credit_Institution");
				this.accountNr = res.getInt("emBankaccount.accountNr");
				this.bin = res.getInt("emBankaccount.bin");
			}
			}catch (Exception e){
		e.printStackTrace();
	}finally{
		try {
			cone.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	}
		
		/**
		 * Konstruktor f�r Mitarbeiter mit ALLEN seinen Daten.
		 * @param vorname, nachname, strasse, telenr, mobilnr, email, day of birth, geschlecht,
		 * staatsangehoerigkeit, plz, ort, land, kontoinh, kreditinstitut, kontonr, blz
		 */
	public Employee(String firstName, String lastName, String street, String tel, String mobil, 
			String email, String dayOfBirth, String sex, String nationality, String city, String country, 
			String depositor, String credit_Institution, int accountNr, int bin){
		this.firstName = firstName;
		this.lastName = lastName;
		this.street = street;
		this.tel = tel;
		this.mobil = mobil;
		this.email = email;
		this.dayOfBirth = dayOfBirth;
		this.sex = sex;
		this.nationality = nationality;
		this.zip = zip;
		this.city = city;
		this.country = country;
		this.depositor = depositor;
		this.credit_Institution = credit_Institution;
		this.accountNr = accountNr;
		this.bin = bin;
	}
			
	/**
	 * Procedure, um den Mitarbeiter in der Datenbank anzulegen.
	 */	
	public void MAanlegenProcedure() {
		Connection cone = ConnectionFactory.createConnection();

		try {
			java.sql.CallableStatement s;
			//Ruft die Procedure auf, damit man die Daten �ber die Mitarbeiter anlegen kann
			s = cone.prepareCall("{call employeeApply (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);}");
			
			s.setString(1, this.firstName);
			s.setString(2, this.lastName);
			s.setString(3, this.street);
			s.setString(4, this.tel);
			s.setString(5, this.mobil);
			s.setString(6, this.email);
			s.setString(7, this.dayOfBirth);
			s.setString(8, this.sex);
			s.setString(9, this.nationality);
			s.setInt(10, this.zip);
			s.setString(11, this.city);
			s.setString(12, this.country);
			s.setString(13, this.depositor);
			s.setString(14, this.credit_Institution);
			s.setInt(15, this.accountNr);
			s.setInt(16, this.bin);
			
			s.execute();
			//die Daten in DaBa rein legen
			cone.commit();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				cone.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
		
/**
 * Procedure, um die aktuellen MA-Daten in die Datenbank zu schreiben.
 */
	
	
	
/**
 * Procedure zum L�schen eines MA.
 */
	
	
	
	/**
	 * Alle Getter und Setter
	 *
	 */
	public int getIdemployee() {
		return idemployee;
	}
	public void setIdemployee(int idemployee) {
		this.idemployee = idemployee;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	
	public String getStreet(){
		return street;
	}
	public void setStreet(String street){
		this.street = street;
	}
	
	public String getTel(){
		return tel;
	}
	public void setTel(String tel){
		this.tel = tel;
	}
	
	public String getMobil(){
		return mobil;
	}
	public void setMobil(String mobil){
		this.mobil = mobil;
	}
	
	public String getEmail(){
		return email;
	}
	public void setEmail(String email){
		this.email = email;
	}
	
	public String getDayOfBirth(){
		return dayOfBirth;
	}
	public void setDayOfBirth(String dayOfBirth){
		this.dayOfBirth = dayOfBirth;
	}
	
	public String getSex(){
		return sex;
	}
	public void setSex(String sex){
		this.sex = sex;
	}
	
	public String getNationality(){
		return nationality;
	}
	public void setNationality(String nationality){
		this.nationality = nationality;
	}
	
	public int getZip(){
		return zip;
	}
	public void setZip(int zip){
		this.zip = zip;
	}
	
	public String getCity(){
		return city;
	}
	public void setCity(String city){
		this.city = city;
	}
	
	public String getCountry(){
		return country;
	}
	public void setCountry(String country){
		this.country = country;
	}
	
	public String getDepositor(){
		return depositor;
	}
	public void setDepositor(String depositor){
		this.depositor = depositor;
	}
	
	public String getCredit_Institution(){
		return credit_Institution;
	}
	public void setredit_Institution(String credit_Institution){
		this.credit_Institution = credit_Institution;
	}
	
	public int getAccountNr(){
		return accountNr;
	}
	public void setAccountNr(int accountNr){
		this.accountNr = accountNr;
	}
	
	public int getBin(){
		return bin;
	}
	public void setBin(int bin){
		this.bin = bin;
	}
	
	public int rowsInResultSet (ResultSet r) throws SQLException {
			r.last();
			int rows = r.getRow();
			r.beforeFirst();
			return rows;
		}
		
	}
	
	
	

