package model;

/** 
 * Zeigt alle Info �ber die Customer und Employee aus der DABA in GUI an
 **/
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class showAllData<d,c> {
	private d data;
	private c column;
	
	public showAllData(d data, c column)
    {
        this.data = data;
        this.column = column;
    }


	public d getData() {
		return data;
	}
	public c getColumn() {
		return column;
	}
	public static  showAllData <Object[][], Object[]> initTable(int zahl){
		Object[] column = null;
		Object[][] data = null;
		String query = null;
		
		switch (zahl){
			//Gast anzeigen in GUI
		case 1:	
			query = "select  c.idcustomer, c.firstName, c.lastName, c.street, c.customerEmai,c.customerPhone, " +
					"c.customerIdentityCard, c.dateOfBirth, c.gender,c.nationality from customer as c " +
					"join cuAddress cad on c.address_idaddress = cad.idaddress";
			break;
			//MA anzeigen
		case 2:
			query = "select e.idemployee, e.firstName, e.lastName, e.street, e.tel, e.mobil, e.email, e.dayOfBirth, e.sex, " +
					"e.nationality from employee as e join emaddress ema on ema.emAddress_idemAddress = e.idemAddress ";
			//query = "select * from mitarbeiter m";
			break;
			/*Reservierung anzeigen
		case 3:
			query="select r.ReservierungsID,r.idZimmer as Zimmernummer,r.Datum as Reservierungsdatum,r.Anzahl_Personen as Personen,z.Zimmerart,r.vonDatum as Von,r.bisDatum Bis,r.Gesamtpreis,ek.Name as Verpflegungsart,ek.Preis as 'Verfplegungspreis/�' from Reservierung r join Essen es on es.idVerpflegung=r.idVerpflegung join Essenskategorie ek on ek.idEssenskategorie=es.IDKategorie join Zimmer z on z.idZimmer=r.idZimmer join Zimmer_has_Preis zp on zp.Zimmer_idZimmer=z.idZimmer join Preis p on p.idPreis=zp.Preis_idPreis group by r.ReservierungsID";
		*/
		default:
			break;
		}
			
		Connection cone = ConnectionFactory.createConnection();

		try {
			// statement erstellen
			Statement state = (Statement) cone.createStatement();
			// Query execution
			ResultSet res = state.executeQuery(query);
	
			// Abrufen von der Metadaten, um den Spaltennamen abzurufen 
			ResultSetMetaData meta = res.getMetaData();

			// Initialisierung eines Arrays f�r den Titel der Matrixspalten Tabellen
			column = new Object[meta.getColumnCount()];
			
			for(int i = 1 ; i <= meta.getColumnCount(); i++){
				column[i-1] = meta.getColumnName(i);
			}
			
			// Abrufen der Anzahl von Zeilen (Ergebnis)
			res.last();

			data = new Object[res.getRow()][meta.getColumnCount()];
			
			// Erhalten der Cursor in der ersten Zeile
			res.beforeFirst();
			int j = 1;
			
			// einf�llen ein array mit den Objecten (Object[][])
			while(res.next()){
			
				for(int i = 1 ; i <= meta.getColumnCount(); i++)
					data[j-1][i-1] = res.getObject(i);
				j++;
			}
			res.close();
			state.close();
			cone.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "ERROR ! ", JOptionPane.ERROR_MESSAGE);
			}

			return new showAllData <Object[][],Object[]>(data, column);
	
	}
}
