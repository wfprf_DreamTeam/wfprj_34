package model;

//Suchen nach einem freien Bett mit dem eingegebenen Datum

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;


public class Bed_Suchen {

	protected ResultSet res;
	
	public Bed_Suchen (Date start, Date end, String bedCategory){
		super();
		
		//DB Connection von der Klasse ConnectionFactory
		Connection cone = ConnectionFactory.createConnection();
		try{
			
			java.sql.Date s=new java.sql.Date(start.getTime());
			java.sql.Date e=new java.sql.Date(end.getTime());
			
			System.out.println(s);
			System.out.println(e);
			System.out.println("Call bedDataSearch('"+s+"','"+e+"','"+bedCategory+"');");
			
			CallableStatement sta = cone.prepareCall("Call bedDataSearch('"+s+"','"+e+"','"+bedCategory+"');",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			//CallableStatement sta = c.prepareCall("CALL bedDataSearch('2013-01-16','2013-01-31','nunc','Semper');",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			res = sta.executeQuery();
			int nbOfRows = rowsInResultSet(res);
			res.first();
			if (nbOfRows != 0)	{
				
						
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		
	}
			
public int rowsInResultSet (ResultSet r) throws SQLException {
	r.last();
	int rows = r.getRow();
	r.beforeFirst();
	return rows; 
	}

public ResultSet getResultSet (){
	
	return res;
	
}
	
}
