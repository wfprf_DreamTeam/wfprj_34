package model;

import java.sql.*;
import java.util.ArrayList;


/**
 *  Diese Klasse ist die Verbindung zur Datenbank 
 **/

public class ConnectionFactory {
	
	private static String url;
	private static String db;
	private static String user;
	private static String pw;
	
	/**
	 *  Diese Prozedur verbindet zur Datenbank...
	 **/
	
public static Connection createConnection(){
	
	url = "jdbc:mysql://i-intra-02.informatik.hs-ulm.de:3306/";
	db = "wfprj_34"; // Database name
	user = "wfprj_34"; // a user on this db
    pw = "wfprj_34";// the password
	
	Connection cone = null;
	try{
		 // Verbindung zur Datenbank
		cone = DriverManager.getConnection(url + db, user, pw);
		cone.setAutoCommit(false);
	}catch(SQLException e){
		System.out.println(e.getMessage());
		e.printStackTrace();
		System.out.println("SQL Expcetion bei Verbindungsaufbau");
	}catch(Exception e){
		System.out.println(e.getMessage());
		e.printStackTrace();
		System.out.println("Connection Fehler");
	}
    return cone;
  }

/**
 * Funktion fuer JUnit
 * @return String: Url + db+ user+ pw
 */
public String getConnectionText(){
	String rueckgabe = url + db + user + pw;
	return rueckgabe;
	}

}
