package model;

/**
 * Erstellt eine JComboboxKlasse, um das Datum separat in den Tagen, Monaten und Jahren auszugeben
 * Hilfsfunktionen f�r die Views
 * Erstellt aus einer Tages, einer Monats, und einer JahresCombobox einen Datumsstring.
 * @param tag: Die ComboBox f�r den Tag...
 * @param monat: "" Monat
 * @param jahr: "" Jahr
 * @return Datumsstring, richtig formatiert f�r java.sql.Date bzw java.util.Date.
 **/

import javax.swing.JComboBox;

public class GuiHilfsklasse {

	public static String ComboBoxDatum(JComboBox tag, JComboBox monat, JComboBox jahr){
		String datum = null;
		//Konwertiren (getSelectedItem()) das Datum aus dem Type Date in String, um SubString anzuwenden
		datum = jahr.getSelectedItem() + "-" + monat.getSelectedItem() + "-" + tag.getSelectedItem();
		return datum;
	}
	/**
	 * Setzt die �bergebenen ComboBoxen auf Datum.
	 * @see GUIhilfsklasse.ComboBoxDatum
	 * @param Datum: Datumsstring, wie er zb aus java.util.Date.getTime() kommt.
	 * @param tag: Combobox f�r den Tag...
	 * @param monat: " Montat
	 * @param jahr: " Jahr
	 **/
	public static void DatumComboBox(String datum, JComboBox tag, JComboBox monat, JComboBox jahr){
		tag.setSelectedItem(datum.substring(8, 10));
		monat.setSelectedItem(datum.substring(5, 7));
		jahr.setSelectedItem(datum.substring(0, 4));
		
	}
	
}
