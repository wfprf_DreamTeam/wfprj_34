package model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class ResultSetToTable {
	
	/**
	 * wandelt ein ResultSet in eine f�r JTables verwertbare Gestalt um
	 */
		private Object[][] data;
		private Object[] column;

		/**
		 * Konstruktor f�r ResultSetToTable
		 * @param res Das Resultset, das in einen Table gesteckt werden soll..
		 */
		public ResultSetToTable(ResultSet res)
	    {
			try{
				System.out.println("test");
			ResultSetMetaData meta = res.getMetaData();
			// Initialize the Column Names
			column = new Object[meta.getColumnCount()];
			for(int i = 1 ; i <= meta.getColumnCount(); i++){
				column[i-1] = meta.getColumnLabel(i);
				System.out.println(meta.getColumnLabel(i));
			}
			// retrieving the number of lines (Results)
			res.last();
			data = new Object[res.getRow()][meta.getColumnCount()];					
			// getting the cursor to the first line :)
			res.beforeFirst();
			int j = 1;
			while(res.next()){
				
				for(int i = 1 ; i <= meta.getColumnCount(); i++)
					data[j-1][i-1] = res.getObject(i);
				j++;
			}
			                      
			res.close();
			}catch (SQLException esql){
				esql.printStackTrace();
			}
			
	}


		public Object[][] getData() {
			return data;
		}


		public Object[] getColumn() {
			return column;
		}
	}
	
	
