package views_javgui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.ImageIcon;
import java.awt.Image;

import views_javgui.MainGUI;
import views_javgui.EmployeeApply;


public class MainGUI extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;
    private static JDesktopPane desktopPane; 
 
    //Variablen von den erstellten Model_Klassen deklariern
    private CardLayout cl;
    private static MainGUI view;
    //private Gastanlegen gasta;
    private EmployeeApply mita;
    //protected Reservierunganlegen rasta;
    //private AlleGastAnzeigen taba;
	//private MitarbeiterAnzeigen mittaba;
    private JPanel contentPanel;
    private static Dimension d;
    private JMenuItem mnEdit_1;
    
 /* Buttons mit Icons
 	Icon iconrot = new ImageIcon(
 			"D:\\HochschuleUlm-Neu-Ulm\\workspace\\wfprj_34\\src\\java_1\\picture.logo.png");
   */   
   
/**
 *  Erzeugung GUI Frame in Konstruktor
 **/
 public MainGUI(){
	 
		 try {
			//Aussehen der Programme k�mmert, Gibt das aktuelle L&F zur�ck.
         for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
             if ("Nimbus".equals(info.getName())) {
             	//Es ist die spezielle Methode setLookAndFeel(), die als Parameter eine Klasse erwartet.
                 //static void setLookAndFeel( String className ) throws ClassNotFoundException, InstantiationException, 
             	//IllegalAccessException, UnsupportedLookAndFeelException - Setzt ein neues L&F.
             	javax.swing.UIManager.setLookAndFeel(info.getClassName());
                 break;
             }
         }
	 } catch (ClassNotFoundException ex) {
         java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
     } catch (InstantiationException ex) {
         java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
     } catch (IllegalAccessException ex) {
         java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
     } catch (javax.swing.UnsupportedLookAndFeelException ex) {
         java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
     } 

	 setFrameproperties();
		
	 createMenu();
		
	 createContentPane();
		
	 createToolbar();
		
	 createDesktopPane();
         
 }
/**
 * Erstellt Men� --- Datei, Bearbeiten, �ber uns
 * @param 
 */
	public void createMenu(){
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnDatei = new JMenu("Datei");
		menuBar.add(mnDatei);
		
		JMenuItem mnEdit = new JMenuItem();
		mnEdit_1 = new JMenu("Bearbeiten");
		menuBar.add(mnEdit_1);
		
	    JMenuItem mntmBenutzerAnlegen = new JMenuItem("Kunde anlegen");
	    
        JMenuItem mntmMitarbeiterAnlegen = new JMenuItem("Mitarbeiter anlegen");
        mntmMitarbeiterAnlegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				  if (mita != null){
		        		mita.dispose();
		    	        }
		        		if ((mita == null) || (mita.isClosed())	){
		    					mita = new EmployeeApply();
		    					
		    					desktopPane.add(mita,"anlegenGast");//"anlegenMitarbeiter"
		    					mita.show();
		    					}
		        
			}
		});
     // Farben f�r die Buttons erstellen
       // mntmMitarbeiterAnlegen.setIcon(iconrot);
     		//rot.setPreferredSize(new Dimension(60, 60));
		//mntmMitarbeiterAnlegen.setIcon(new ImageIcon(MainGUI.class.getResource("/java_1/pictureValid.png")));
		mnEdit_1.add(mntmMitarbeiterAnlegen);
		
        
	    JMenuItem mntmTabelleanzeigen = new JMenuItem("Kundentabelle anzeigen");
	    
	    JMenuItem mntmTabelleanzeigenmita = new JMenuItem("Mitarbeitertabelle anzeigen");
	    
	    
	    mntmBenutzerAnlegen.addActionListener(this);
	    //mntmMitarbeiterAnlegen.addActionListener(this);
	    mntmTabelleanzeigen.addActionListener(this);
	    mntmTabelleanzeigenmita.addActionListener(this);
	    
	    mnEdit_1.add(mntmBenutzerAnlegen);
	    mnEdit_1.add(mntmMitarbeiterAnlegen);
	    mnEdit_1.add(mntmTabelleanzeigen);
	    mnEdit_1.add(mntmTabelleanzeigenmita);
	    
	    
		JMenu mnberUns = new JMenu("\u00DCber Uns");
		menuBar.add(mnberUns);
				
		JMenuItem menuItem = new JMenuItem("");
		menuBar.add(menuItem);
				
	}
 
 
    //Frame Erstellung ----- Frame Eigenschaften
	private void setFrameproperties(){
		
		//setIcon(new ImageIcon(MainGUI.class.getResource("java_1/picture/logo.png")));
		//setIconImage(Toolkit.getDefaultToolkit().getImage(MainGUI.class.getResource("java_1/picture/logo.png")));
		setTitle("HostelMostel");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 30, 720, 720);
		setSize(950,700);    
	}
	
	// create Layout vom Frame
		private void createContentPane(){
			contentPanel = new JPanel();
			setContentPane(contentPanel);
			//Farbe von der Frame
			contentPanel.setBackground(new Color(236, 222, 222));
	        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.X_AXIS));
	       	}
		 //(224, 255, 255)
  /**
   * The main part ------ Create the tool ----------
   */
  	private void createToolbar(){
  		
  		//Layout der Activit�ten von der Linke seite
  		JToolBar toolBar = new JToolBar();
        toolBar.setOrientation(SwingConstants.VERTICAL);
        toolBar.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        toolBar.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
        toolBar.setLayout(new GridLayout (13,1));
        contentPanel.add(toolBar);
  	
  		//Button in Layout anlegen
        JButton btnGastanlegen = new JButton("Kunde anlegen");
        toolBar.add(btnGastanlegen);
        btnGastanlegen.setHorizontalAlignment(SwingConstants.LEFT);
        
        JButton btnMitarbeiteranlegen = new JButton("Mitarbeiter anlegen");
        toolBar.add(btnMitarbeiteranlegen);
        btnMitarbeiteranlegen.setHorizontalAlignment(SwingConstants.LEFT);
            btnMitarbeiteranlegen.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
    	        if (mita != null){
        		mita.dispose();
    	        }
        		if ((mita == null) || (mita.isClosed())	){
    					mita = new EmployeeApply();
    					desktopPane.add(mita,"anlegenGast");
    					mita.show();
    					}
        	}
        });
        
    //btnMitarbeiteranlegen.setIcon(new ImageIcon(MainGui.class.getResource("/java/guiresources/User2.png")));
        
        
        JButton btnReservierung = new JButton("Reservierung anlegen");
        toolBar.add(btnReservierung);
        btnReservierung.setHorizontalAlignment(SwingConstants.LEFT);
        
        JButton btnReporting = new JButton("Reporting");
        toolBar.add(btnReporting);
        btnReporting.setHorizontalAlignment(SwingConstants.LEFT);
        
        JButton btnTabelleanzeigen = new JButton("Eingetragene Kunden");
        toolBar.add(btnTabelleanzeigen);
        btnTabelleanzeigen.setHorizontalAlignment(SwingConstants.LEFT);
        
        JButton btnMitTabelleanzeigen = new JButton("Eingetragene Mitarbeiter");
        toolBar.add(btnMitTabelleanzeigen);
        btnMitTabelleanzeigen.setHorizontalAlignment(SwingConstants.LEFT);
        
        JButton btnReservierungAnzeigen = new JButton("Reservierung Anzeigen");
        toolBar.add(btnReservierungAnzeigen);
        btnReservierungAnzeigen.setHorizontalAlignment(SwingConstants.LEFT);
        
        JButton btnZimmerbelegungsplan = new JButton("Zimmerbelegungsplan");
        toolBar.add(btnZimmerbelegungsplan);
        btnZimmerbelegungsplan.setHorizontalAlignment(SwingConstants.LEFT);
        
        
        
        btnGastanlegen.addActionListener(this);
        //btnMitarbeiteranlegen.addActionListener(this);
        btnReservierung.addActionListener(this);
        btnReporting.addActionListener(this);
        btnTabelleanzeigen.addActionListener(this);
        btnMitTabelleanzeigen.addActionListener(this);
        btnReservierungAnzeigen.addActionListener(this);
        btnZimmerbelegungsplan.addActionListener(this);
        
  	}
  	
  	private void createDesktopPane(){
        desktopPane = new JDesktopPane();
        desktopPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        desktopPane.setBackground(SystemColor.menu);
        contentPanel.add(desktopPane);
        desktopPane.setLayout(null);
	}
	//SystemColor.menu
    public static JDesktopPane getDesktopPane() {
		return desktopPane;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}    
        
        
 
    }



