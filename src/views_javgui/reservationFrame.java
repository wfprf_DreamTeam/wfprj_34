package views_javgui;


import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


//import com.toedter.calendar.JDateChooser;
//import com.toedter.calendar.JDateChooserCellEditor;
import javax.swing.JSlider;
import javax.swing.JProgressBar;

// hier wird die gui f�r reservation erstellt 


public class reservationFrame extends JInternalFrame {
	

	//protected Calendar cal;
	protected JLabel lblAnreise;
	protected JLabel lblAbreise;
	protected JLabel lblZimmerart;
	protected JLabel lblBettart;
	protected JPanel contentPane;
	protected JTextField txtAnreise;
	protected JTextField txtAbreise;
	protected JButton btnOK;
	protected JButton btnAbbrechen;
	private JComboBox comboBox_1;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					reservationFrame frame = new reservationFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public reservationFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 506, 365);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 0, 0);
		contentPane.add(panel);
		
		// JLabel
		
		lblAnreise = new JLabel("Anreise");
		lblAnreise.setBounds(10, 11, 46, 14);
		contentPane.add(lblAnreise);
		
		lblAbreise = new JLabel("Abreise");
		lblAbreise.setBounds(223, 11, 46, 14);
		contentPane.add(lblAbreise);
		
		lblZimmerart = new JLabel("Zimmerart");
		lblZimmerart.setBounds(10, 49, 76, 14);
		contentPane.add(lblZimmerart);
		
		lblBettart = new JLabel("Bettart");
		lblBettart.setBounds(223, 49, 86, 14);
		contentPane.add(lblBettart);


		
		
		// Textfelder
		txtAnreise= new JTextField();
		txtAnreise.setBounds(57, 8, 86, 20);
		contentPane.add(txtAnreise);
		txtAnreise.setColumns(10);
		
		txtAbreise = new JTextField();
		txtAbreise.setBounds(279, 8, 86, 20);
		contentPane.add(txtAbreise);
		txtAbreise.setColumns(10);
		
	
		// Button OK 
		
		btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try{
					okButtonPressed();
				}catch (Exception err){
					err.printStackTrace();
				}
			
			}

			

				protected void okButtonPressed(){
					System.out.println("okbuttonp");
					if (reservieren())
						dispose();
			
				
				
			}



				private boolean reservieren() {
					// TODO Auto-generated method stub
					return false;
				}
		});
		btnOK.setBounds(23, 274, 89, 23);
		contentPane.add(btnOK);
		
		
		
		
		// Button Abbrechen 
		
		btnAbbrechen = new JButton("Abbrechen ");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			contentPane.setVisible(false);
			dispose();
			}
		});
		btnAbbrechen.setBounds(356, 274, 97, 23);
		contentPane.add(btnAbbrechen);
		
		
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(81, 46, 119, 20);
		contentPane.add(comboBox);
		
		comboBox_1 = new JComboBox();
		comboBox_1.setBounds(281, 46, 131, 20);
		contentPane.add(comboBox_1);
		
		// Tabelle 
		
		table = new JTable();
		table.setBounds(23, 103, 440, 146);
		contentPane.add(table);
		
		
		
		
	
		
		
	}

	public boolean isClosed() {
		// TODO Auto-generated method stub
		return false;
	}
}
