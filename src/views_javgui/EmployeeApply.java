package views_javgui;

import javax.swing.ButtonGroup;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.beans.PropertyVetoException;
import java.util.GregorianCalendar;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.CardLayout;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

import model.Customer;

import java.awt.Color;
import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.GregorianCalendar;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

//import javgui.views.BezahlartBankeinzug;
//import javgui.views.BezahlartKreditkarte;
//import javgui.views.Gastanlegen;

//import javgui.views.BezahlartBankeinzug;

import model.Customer;
import model.Employee;


public class EmployeeApply extends JInternalFrame{
	
	protected JTextField vorName;
	protected JTextField nachName;
	protected JTextField stra�e;
	protected JTextField plz;
	protected JTextField land;
	protected JTextField stadt;
	protected JTextField festnetz;
	protected JTextField mobil;
	protected JTextField email;
	protected JTextField Ausweisnr;
	public JComboBox cbxmonth = new JComboBox();
	public JComboBox cbxyear = new JComboBox();
	public JComboBox cbxinsday = new JComboBox();
	protected JTextField Staatsangeh;
	protected String geschlecht = null;
	protected JComboBox geschlechtcomboBox;
	//protected BezahlartKreditkarte bezahlartKreditkarte = new BezahlartKreditkarte();
	//protected BezahlartBankeinzug bezahlartBankeinzug = new BezahlartBankeinzug();
	//protected CardLayout cl_panel;
	protected ButtonGroup b1;
	//protected JRadioButton rdbtnKreditkarte;
	//protected JRadioButton rdbtnBankeinzug;
	
	//Labels
	protected JLabel lblGeschlecht;
	protected JLabel lblVorname;
	protected JLabel lblNachname;
	protected JLabel lblGeburtsdatum;
	protected JLabel lblStrae;
	protected JLabel lblPlz;
	protected JLabel lblLand;
	protected JLabel lblStadt;
	protected JLabel lblTelefonnummer;
	protected JLabel lblMobil;
	protected JLabel lblEmail;
	protected JLabel lblStaatsangehrigkeit;
	protected JLabel lblAusweisnr;
	protected JLabel lblKreditinstitut;
	protected JButton btnOk;
	protected JButton btnAbbrechen;
	
	Employee ma; 
	
	/**
	 * Create the panel.
	 */
	public EmployeeApply() {
	setTitle("Mitarbeiteranlegen");
	
	//Gr�sse anpassen 
	setResizable(true);
	setIconifiable(true);
	setMaximizable(true);
	setClosable(true);
	getContentPane().setBackground(new Color(236, 222, 222));
	getContentPane().setLayout(null);
	
	//Layout Grenze
	setBounds(0, 0, 720, 500);
	
	lblVorname = new JLabel("Vorname");
	lblVorname.setBounds(10, 60, 62, 14);
	getContentPane().add(lblVorname);
	
	final JPanel panel = new JPanel();
	panel.setBounds(235, 142, 409, 170);
	panel.setBackground(new Color(236, 222, 222));
	getContentPane().add(panel);
	panel.setLayout( new CardLayout(0, 0));

	//JCardLayout
	//panel.add(bezahlartKreditkarte, "Kreditkarte");
	//panel.add(bezahlartBankeinzug, "Bankeinzug");
	
	final CardLayout cl_panel = (CardLayout) (panel.getLayout());
	cl_panel.show(panel, "Bankeinzug");
	
	vorName = new JTextField();
	vorName.setBounds(72, 52, 153, 28);
	getContentPane().add(vorName);
	vorName.setColumns(10);
	
	lblNachname = new JLabel("Nachname");
	lblNachname.setBounds(10, 92, 62, 14);
	getContentPane().add(lblNachname);
	
	nachName = new JTextField();
	nachName.setBounds(72, 85, 153, 28);
	getContentPane().add(nachName);
	nachName.setColumns(10);
	
	lblStrae = new JLabel("Stra\u00DFe");
	lblStrae.setBounds(10, 125, 45, 14);
	getContentPane().add(lblStrae);
	
	stra�e = new JTextField();
	stra�e.setBounds(72, 118, 153, 28);
	getContentPane().add(stra�e);
	stra�e.setColumns(10);
	
	lblPlz = new JLabel("PLZ");
	lblPlz.setBounds(10, 158, 46, 14);
	getContentPane().add(lblPlz);
	
	plz = new JTextField();
	plz.setBounds(72, 151, 153, 28);
	getContentPane().add(plz);
	plz.setColumns(10);
	
	lblStadt = new JLabel("Stadt");
	lblStadt.setBounds(9, 191, 46, 14);
	getContentPane().add(lblStadt);
	
	stadt = new JTextField();
	stadt.setBounds(72, 184, 153, 28);
	getContentPane().add(stadt);
	stadt.setColumns(10);
	
	lblLand = new JLabel("Land");
	lblLand.setBounds(9, 224, 46, 14);
	getContentPane().add(lblLand);
	
	land = new JTextField();
	land.setBounds(72, 217, 153, 28);
	getContentPane().add(land);
	land.setColumns(10);
			
	lblTelefonnummer = new JLabel("Festnetz");
	lblTelefonnummer.setBounds(10, 257, 62, 14);
	getContentPane().add(lblTelefonnummer);
	
	festnetz = new JTextField();
	festnetz.setBounds(72, 250, 153, 28);
	getContentPane().add(festnetz);
	festnetz.setColumns(10);
	
	lblMobil = new JLabel("Mobil");
	lblMobil.setBounds(9, 290, 46, 14);
	getContentPane().add(lblMobil);
	
	mobil = new JTextField();
	mobil.setBounds(72, 283, 153, 28);
	getContentPane().add(mobil);
	mobil.setColumns(10);
	
	lblEmail = new JLabel("E-Mail");
	lblEmail.setBounds(9, 323, 46, 14);
	getContentPane().add(lblEmail);
	
	email = new JTextField();
	email.setBounds(72, 316, 153, 28);
	getContentPane().add(email);
	email.setColumns(10);
	
	lblGeschlecht = new JLabel("Geschlecht");
	lblGeschlecht.setBounds(10, 30, 63, 14);
	getContentPane().add(lblGeschlecht);
	
	geschlechtcomboBox = new JComboBox();
	
	/*// Array f�r unsere JComboBox
        String comboBoxListe[] = {"W\u00E4hlen Sie Geschlecht", "M\u00E4nnlich", "Weiblich"};
         //JComboBox mit Bundesl�nder-Eintr�gen wird erstellt
        geschlechtcomboBox.add(comboBoxListe); */
	
	//Dort wird z.B. definiert, welche Daten in der Klappliste 
	//angezeigt werden und welcher Eintrag selektiert ist. Standardm��ig ist die Klasse DefaultComboBoxModel daf�r zust�ndig. 
	geschlechtcomboBox.setModel(new DefaultComboBoxModel(new String[] {"W\u00E4hlen Sie Geschlecht", "M\u00E4nnlich", "Weiblich"}));
	geschlechtcomboBox.setSelectedIndex(0);
	geschlechtcomboBox.setBounds(72, 24, 153, 25);
	getContentPane().add(geschlechtcomboBox);
	
		
	btnOk = new JButton("OK");
	// NEUER ACTIONLISTENER
	btnOk.addActionListener(new ActionListener() {
				
		@Override
		public void actionPerformed(ActionEvent e) {
			try{
				//okButtonPressed();
			}catch (Exception err){
				err.printStackTrace();
			}
			}
		});
		
		btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();// anstatt System.exit(0)
			}
		});
		
		btnOk.setHorizontalAlignment(SwingConstants.LEFT);
		//btnOk.setIcon(new ImageIcon(Gastanlegen.class.getResource("/java/guiresources/Valid.png")));
		btnOk.setBounds(72, 377, 138, 39);
		getContentPane().add(btnOk);
		
		
		//btnAbbrechen.setIcon(new ImageIcon(Gastanlegen.class.getResource("/java/guiresources/Error.png")));
		btnAbbrechen.setHorizontalAlignment(SwingConstants.LEFT);
		btnAbbrechen.setBounds(255, 377, 138, 39);
		getContentPane().add(btnAbbrechen);
		
		lblGeburtsdatum = new JLabel("Geburtsdatum");
		lblGeburtsdatum.setBounds(255, 30, 110, 14);
		getContentPane().add(lblGeburtsdatum);
		
		lblStaatsangehrigkeit = new JLabel("Staatsangeh\u00F6rigkeit");
		lblStaatsangehrigkeit.setBounds(255, 60, 110, 14);
		getContentPane().add(lblStaatsangehrigkeit);
	}
	
	/*protected void okButtonPressed(){
		
		try{
		ma = new Employee(
				vorName.getText(),
				nachName.getText(),
				stra�e.getText(),
				festnetz.getText(),
				mobil.getText(),
				email.getText(),
				model.GuiHilfsklasse.ComboBoxDatum(cbxinsday,cbxmonth,cbxyear),
				((String)geschlechtcomboBox.getSelectedItem()).substring(0,1),
				Staatsangeh.getText(),
				Integer.parseInt(plz.getText()),
				stadt.getText(),
				land.getText()
				
				/*bezahlartBankeinzug.getTextFieldKontoinh(),
				bezahlartBankeinzug.getTextFieldKreditinst(),
				bezahlartBankeinzug.getTextFieldKtoNr(),
				bezahlartBankeinzug.getTextFieldBLZ()
		
		);
		ma.MAanlegenProcedure();
		}catch (Error e){
				JOptionPane.showMessageDialog(null,e.getMessage(),"Error!",JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
				e.printStackTrace();
		}
		
		dispose();
*/
}
	
				
		
		
			
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	